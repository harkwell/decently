Decently
=================
Overview
---------------
A very inexpensive, secure, serverless platform leveraging Amazon Web Services
to coordinate christian worship services.

Deployment
---------------
```shell
BUCKET="my-worldwide-unique-bucketname"
bash build.sh
bash deploy.sh $BUCKET
chromium-browser http://aws.amazon.com/  # log into console
# box -> cognito -> "manage user pools" -> "create" -> "decently" -> "review..."
   NO, have deploy.sh do it via aws-cli
```


URLs
---------------
* http://editor.swagger.io/#/
* http://swagger.io/specification/
* http://docs.aws.amazon.com/AWSJavaSDK/latest/javadoc/index.html
* http://docs.aws.amazon.com/cognito/latest/developerguide/authentication-flow.html
* http://docs.aws.amazon.com/amazondynamodb/latest/gettingstartedguide/GettingStarted.Java.02.html
* https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS
* http://www.w3.org/TR/cors/
* http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-mapping-template-reference.html

Notes
---------------
# dump the RESTful API definition in yaml

```shell
APIID=$(aws apigateway get-rest-apis |jq '.items | .[] |select(.name == "Decently") | .id' |sed 's#"##g')
aws apigateway get-stages --rest-api-id $APIID
aws apigateway get-export --rest-api-id $APIID --stage-name dev --export-type swagger /tmp/decently.yaml
```
