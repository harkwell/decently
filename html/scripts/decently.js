function postWorker(worker)
{
   var retval;
   var json = JSON.stringify(worker);
   
   console.log("posting json: "+json);
   $.ajax({
      type: "POST",
      url: "APIURL/workers",
      data: ""+json,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      statusCode: {
         401: function (data) {
            console.log("basic auth required");
         }
      },
      success: function (data, status, jqXHR) {
         retval = data;
      },
      error: function (xhr) {
         alert(xhr.responseText);
      }
   });
   return retval;
};
