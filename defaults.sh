#!/bin/bash

AWSREGION=us-west-2
API_ID=$(aws apigateway get-rest-apis |jq '.items[] |select(.name == "Decently") | .id' |sed 's#"##g')
APIURL=$(echo "https://$API_ID.execute-api.$AWSREGION.amazonaws.com/dev")

#############
### ROLES ###
#############
for role in Preacher Song_Leader Leader_of_Prayer Small_Group_Leader_of_Prayer Maker_of_Announcements Teacher Adult_Bible_Class_Teacher Nursery_Age_Teacher Highschool_Age_Teacher Scripture_Reader Money_Counter Usher Audio_System_Administrator Door_Greeter Communion_Preparer Lords_Supper_Leader Lords_Supper_Helper; do
   name=`echo $role |sed 's#_# #g'`
   curl -s -X POST -H "Accept:application/json" -H "Content-Type:application/json" $APIURL/roles -d "{ \"name\" : \"$name\", \"description\" : \"$name\" }"
done
