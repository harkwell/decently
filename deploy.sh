#!/usr/bin/bash

D="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

BUCKET=$1
KEY=$(basename `tempfile --prefix=tmp-`)
TMPFILE=$(tempfile)
AWSREGION=us-west-2

echo perform sanitchy check and provision script
[ -z "$BUCKET" ] && { echo "must specify bucket name!"; exit 1; }

aws s3 ls s3://$BUCKET 2>&1 >/dev/null || {
    echo "aws bucket must already exist: s3://$BUCKET/"
    exit 1;
}
cp swagger/swagger.yaml.part swagger.yaml
printf '\npaths:\n' >>swagger.yaml

for x in swagger/paths*part; do
   cat $x |sed 's#^#    #' >>swagger.yaml
done
printf '\ndefinitions:\n' >>swagger.yaml

for x in swagger/defs*part; do
   cat $x |sed 's#^#    #' >>swagger.yaml
done
sed -i -e "s#AWSREGION#$AWSREGION#g" swagger.yaml
aws s3 cp $D/swagger.yaml s3://$BUCKET/$KEY/
aws s3 cp $D/target/decently-0.1.1.jar s3://$BUCKET/$KEY/

echo create aws iam role: decently-role
aws iam create-role --role-name decently-role --assume-role-policy-document '{ "Version": "2012-10-17", "Statement": [ { "Sid" : "", "Effect" : "Allow", "Principal" : { "Service": ["ec2.amazonaws.com", "apigateway.amazonaws.com", "lambda.amazonaws.com" ] }, "Action": [ "sts:AssumeRole" ] } ] }' 2>&1 >/dev/null
aws iam attach-role-policy --role-name decently-role --policy-arn arn:aws:iam::aws:policy/AdministratorAccess
IAMROLEARN=$(aws iam list-roles |jq '.Roles[] |select(.RoleName == "decently-role") | .Arn' |sed 's#"##g')
echo "        decently-role: \"$IAMROLEARN\""
echo "        Wait for role to become available..."
sleep 10

LINE=$(grep -n INSERTSTANZAS $D/AWS-cloudformation.json.tmpl |cut -d: -f1)
sed -n 1,$((LINE - 1))p $D/AWS-cloudformation.json.tmpl >$TMPFILE
count=0

for table in services timeslots duties timeslot_duties workers roles \
      service_timeslots worker_roles; do
   count=$((count + 1))
   cat <<EOF >>$TMPFILE
      "table${count}" : {
         "Type"       : "AWS::DynamoDB::Table",
         "Properties" : {
            "TableName"             : "dctly_${table}",
            "ProvisionedThroughput" : {
                "ReadCapacityUnits"  : "1",
                "WriteCapacityUnits" : "1"
            },
            "AttributeDefinitions"  : [
                {
                    "AttributeName" : "guid",
                    "AttributeType" : "S"
                }
            ],
            "KeySchema"             : [
                {
                    "AttributeName" : "guid",
                    "KeyType"       : "HASH"
                }
            ]
         }
      },
EOF
done

sed -n $((LINE + 1)),\$p $D/AWS-cloudformation.json.tmpl >>$TMPFILE
sed -i -e "s#<S3BUCKET>#$BUCKET#g" -e "s#<S3KEY>#$KEY#g" \
    -e "s#<IAMROLE>#$IAMROLEARN#g" $TMPFILE
aws cloudformation create-stack --stack-name decently \
    --capabilities CAPABILITY_IAM \
    --tags 'Key=projects,Value=decently' \
    --template-body file://$TMPFILE

rm $TMPFILE
echo waiting on aws cloud to form
aws cloudformation wait stack-create-complete || {
    echo AWS cloud formation failed to provision decently
    exit 1
}

echo replace hard-coded references to aws resources
API_ID=$(aws apigateway get-rest-apis |jq '.items[] |select(.name == "Decently") | .id' |sed 's#"##g')
echo "        api id: \"$API_ID\""
APIURL=$(echo "https://$API_ID.execute-api.$AWSREGION.amazonaws.com/dev")
echo "        api url: \"$APIURL\""
find html -type f -exec sed -i -e "s#API_ID#$API_ID#g" -e "s#APIURL#$APIURL#g" -e "s#AWSREGION#$AWSREGION#g" {} \;

echo stand-up cognito service
aws cognito-idp create-user-pool --pool-name decently --auto-verified-attributes email 2>&1 >/dev/null
POOLID=$(aws cognito-idp list-user-pools --max-results 10 |jq '.UserPools[] |select(.Name == "decently") | .Id' |sed 's#"##g')
echo "        user pool id: \"$POOLID\""

echo import users.csv users into cognito
URL=$(aws cognito-idp create-user-import-job --job-name decently --user-pool-id $POOLID --cloud-watch-logs-role-arn $IAMROLEARN |jq '.UserImportJob |.PreSignedUrl' |sed 's#"##g')
echo "        time sensitive signed url: \"$URL\""
curl -T users.csv -H "x-amz-server-side-encryption:aws:kms" "$URL"


echo push html to bucket
aws s3 cp --recursive --acl public-read html s3://$BUCKET/html
aws s3 rm --recursive s3://$BUCKET/$KEY
aws s3 website --index-document index.html s3://$BUCKET
#aws s3api put-bucket-cors --bucket $BUCKET --cors-configuration file:///tmp/cors.json

for x in $(aws s3 ls s3://$BUCKET/tmp* |cut -d\- -f2); do
   aws s3 rm --recursive s3://$BUCKET/tmp-$x
done

echo create the database schema

echo SUCCESSFULLY DEPLOYED "decently", connect here:
echo chromium-browser http://$BUCKET.s3-website-$AWSREGION.amazonaws.com/html/index.html
