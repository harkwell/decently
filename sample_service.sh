#!/bin/bash

# NOTE: run defaults.sh first

AWSREGION=us-west-2
API_ID=$(aws apigateway get-rest-apis |jq '.items[] |select(.name == "Decently") | .id' |sed 's#"##g')
APIURL=$(echo "https://$API_ID.execute-api.$AWSREGION.amazonaws.com/dev")

[ $# -gt 0 ] || {
	echo service date is required
	echo example: $0 \"$(date -Ihours)\"
	exit 1
}
DATE=$1

curl -s -X POST -H "Accept:application/json" -H "Content-Type:application/json" $APIURL/services -d "{ \"name\" : \"Sunday Morning Worship\", \"description\" : \"Sunday Morning Worship Service For $(date --date="$DATE" +"%B %d, %Y")\", \"date\" : \"$DATE\" }"
SVC_ID=$(curl -s -X GET -H "Accept:application/json" -H "Content-Type:application/json" $APIURL/services |sed -e 's#"##g' -e 's#\\#"#g' |jq ".dctly_services[] |select(.date == \"$DATE\") |.guid" |sed 's#"##g')

for slot in Announcements:5:Maker_of_Announcements@Speaker First_Prayer:10:Leader_of_Prayer@Speaker First_Songs:20:Song_Leader@Singer Lords_Supper:30:Lords_Supper_Leader@Position_1:Lords_Supper_Leader@Position_2:Lords_Supper_Leader@Position_3:Lords_Supper_Leader@Position_4:Lords_Supper_Helper@Position_A:Lords_Supper_Helper@Position_B:Lords_Supper_Helper@Position_C:Lords_Supper_Helper@Position_D Sermon:45:Preacher@Preaching Closing_Songs:55:Song_Leader@Singer Closing_Prayer:60:Leader_of_Prayer@Speaker; do
	NAME=$(echo $slot |cut -d: -f1 |sed 's#_# #g')
	END=$(echo $slot |cut -d: -f2)
	PTR=$(date --date="$DATE + $END minutes" -Iseconds)
	curl -s -X POST -H "Accept:application/json" -H "Content-Type:application/json" $APIURL/timeslots -d "{ \"name\" : \"$NAME\", \"start\" : \"$DATE\", \"end\" : \"$PTR\" }"
	TMSLT_ID=$(curl -s -X GET -H "Accept:application/json" -H "Content-Type:application/json" $APIURL/timeslots |sed -e 's#"##g' -e 's#\\#"#g' |jq ".dctly_timeslots[] |select(.name == \"$NAME\" and .start == \"$DATE\") |.guid" |sed 's#"##g')
	printf "\ncreated timeslot: $TMSLT_ID for service: $SVC_ID\n"
	curl -s -X POST -H "Accept:application/json" -H "Content-Type:application/json" $APIURL/service_timeslots -d "{ \"service_id\" : \"$SVC_ID\", \"timeslot_id\" : \"$TMSLT_ID\" }"

	for role_duty in $(echo $slot |cut -d: -f3- |tr ':' ' '); do
		ROLE_NAME=$(echo $role_duty |cut -d@ -f1 |tr '_' ' ')
		DUTY_NAME=$(echo $role_duty |cut -d@ -f2 |tr '_' ' ')
		ROLE_ID=$(curl -s -X GET -H "Accept:application/json" -H "Content-Type:application/json" $APIURL/roles |sed -e 's#"##g' -e 's#\\#"#g' |jq ".dctly_roles[] |select(.name == \"$ROLE_NAME\") |.guid" |sed 's#"##g')
		[ "$ROLE_ID" ] || { echo role $ROLE_NAME is missing; exit 1; }
		printf "\ncreating duty: \"$DUTY_NAME\" for role \"$ROLE_NAME\" (id=$ROLE_ID)\n"
		curl -s -X POST -H "Accept:application/json" -H "Content-Type:application/json" $APIURL/duties -d "{ \"name\" : \"$DUTY_NAME\", \"description\" : \"$DUTY_NAME\", \"role_id\" : \"$ROLE_ID\" }"
		DUTY_ID=$(curl -s -X GET -H "Accept:application/json" -H "Content-Type:application/json" $APIURL/duties |sed -e 's#"##g' -e 's#\\#"#g' |jq ".dctly_duties[] |select(.name == \"$DUTY_NAME\" and .role_id == \"$ROLE_ID\") |.guid" |sed 's#"##g')
		printf "\ncreating timeslot duty with timeslot_id=\"$TMSLT_ID\" and duty_id=\"$DUTY_ID\"\n"
		curl -s -X POST -H "Accept:application/json" -H "Content-Type:application/json" $APIURL/timeslot_duties -d '{ "timeslot_id" : "'$TMSLT_ID'", "duty_id" : "'$DUTY_ID'", "worker_id" : "0" }'
	done
done
