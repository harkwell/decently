#!/usr/bin/bash

D="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

BUCKET=$1
AWSREGION=us-west-2

echo perform sanitchy check and provision script
[ -z "$BUCKET" ] && { echo "must specify bucket name!"; exit 1; }

aws s3 ls s3://$BUCKET 2>&1 >/dev/null || {
    echo "aws bucket must already exist: s3://$BUCKET/"
    exit 1;
}

aws s3 rm --recursive s3://$BUCKET/html
API_ID=$(aws apigateway get-rest-apis |jq '.items[] |select(.name == "Decently") | .id' |sed 's#"##g')
APIURL=$(echo "https://$API_ID.execute-api.$AWSREGION.amazonaws.com/dev")
find html -type f -exec sed -i -e "s#API_ID#$API_ID#g" -e "s#APIURL#$APIURL#g" -e "s#AWSREGION#$AWSREGION#g" {} \;
aws s3 cp --recursive --acl public-read html s3://$BUCKET/html
aws s3 website --index-document index.html s3://$BUCKET

echo SUCCESSFULLY DEPLOYED "decently", connect here:
echo chromium-browser http://$BUCKET.s3-website-$AWSREGION.amazonaws.com/html/index.html
