package com.khallware.decently;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.amazonaws.services.dynamodbv2.document.ScanFilter;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Util
{
	public static final String DEF_ENDPOINT =
		"https://dynamodb.us-west-2.amazonaws.com";

	public static AmazonDynamoDBClient getAmazonDynamoDBClient()
			throws IllegalStateException
	{
		return(new AmazonDynamoDBClient().withEndpoint(DEF_ENDPOINT));
	}

	public static DynamoDB getDynamoDB() throws IllegalStateException
	{
		return(new DynamoDB(getAmazonDynamoDBClient()));
	}

	public static Table getTable(String name) throws IllegalStateException
	{
		return(getDynamoDB().getTable(name));
	}

	public static ItemCollection<ScanOutcome> find(
			String table, String key, String val)
			throws IllegalStateException
	{
		return(getTable(table).scan(new ScanFilter(key).eq(val)));
	}

	public static String toJson(Object object) throws IOException
	{
		return(new ObjectMapper().writeValueAsString(object));
	}

	public static String fixJSON(String json)
	{
		String retval = (true) ? json : json.replaceAll("\"", "");
		// retval = retval.replaceAll("\\\\", "\"");
		retval = retval.replaceAll("\\\\\"", "\"");
		retval = retval.replaceAll("\\\\", "");
		retval = retval.replaceAll("[\"{", "[{");
		retval = retval.replaceAll("}\"]", "}]");
		retval = retval.replaceAll(",\"{", ",{");
		retval = retval.replaceAll("}\",", "},");
		retval = (retval.startsWith("\"{"))
			? retval.substring(1)
			: retval;
		retval = (retval.endsWith("}\""))
			? retval.substring(0,retval.length() - 1)
			: retval;
		return(retval);
	}

	public static Map<String,Object> jsonToMap(String json)
			throws IOException
	{
		return(new ObjectMapper().readValue(json, HashMap.class));
	}

	public static void log(Context ctxt, String message)
	{
		if (ctxt != null) {
			ctxt.getLogger().log(message+"\n");
		}
	}
}
