package com.khallware.decently;

public class WorkerRole extends DecentlyRequestHandler implements Item
{
	public enum Keys { guid, worker_id, role_id };

	public WorkerRole()
	{
		super("worker_roles", Keys.values());
	}
}
