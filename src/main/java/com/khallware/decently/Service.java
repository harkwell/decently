package com.khallware.decently;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.dynamodbv2.document.Item;
import java.util.ArrayList;
import java.util.List;

public class Service extends DecentlyRequestHandler
		implements com.khallware.decently.Item
{
	public enum Keys { guid, name, description, date };

	public Service()
	{
		super("services", Keys.values());
	}

	@Override
	public String read(String guid, Context ctxt)
	{
		Item retval = null;
		try {
			retval = appendTimeslots(
				ctxt,
				Util.getTable(table).getItem("guid", guid));
		}
		catch (Exception e) {
			retval = (retval == null) ? new Item() : retval;
			retval.with("errors", ""+e);
		}
		Util.log(ctxt, "service (after): "+retval.toJSON());
		return(Util.fixJSON(retval.toJSON()));
	}

	private Item appendTimeslots(Context ctxt, Item service)
	{
		List<String> timeslots = new ArrayList<>();
		String key = "service_id";
		String val = ""+service.get("guid");
		String table = Configuration.DB_TABLE_PREFIX
			+"service_timeslots";
		Util.log(ctxt, "service (before): "+service.toJSON());

		for (Item item : Util.find(table, key, val)) {
			Item timeslot = null;
			Util.log(ctxt,"found service_timeslot: "+item.toJSON());
			timeslot = getTimeslot(""+item.get("timeslot_id"));
			appendDuties(ctxt, timeslot);
			Util.log(ctxt, "timeslot (after): "+timeslot.toJSON());
			timeslots.add(Util.fixJSON(timeslot.toJSON()));
		}
		Util.log(ctxt, "appending "+timeslots.size()+" timeslots");
		return(service.withList("timeslots", timeslots));
	}

	private Item appendDuties(Context ctxt, Item timeslot)
	{
		List<String> duties = new ArrayList<>();
		String key = "timeslot_id";
		String val = ""+timeslot.get("guid");
		String table = Configuration.DB_TABLE_PREFIX
			+"timeslot_duties";
		Util.log(ctxt, "timeslot (before): "+timeslot.toJSON());

		for (Item item : Util.find(table, key, val)) {
			Util.log(ctxt, "found timeslot_duty: "+item.toJSON());
			String guid = ""+item.get("worker_id");
			Item duty = getDuty(""+item.get("duty_id"));
			Util.log(ctxt, "duty: "+duty.toJSON());
			duty = duty.withJSON("worker", getWorker(guid));
			duties.add(Util.fixJSON(duty.toJSON()));
		}
		Util.log(ctxt, "appending "+duties.size()+" duties");
		return(timeslot.withList("duties", duties));
	}

	private Item getItem(String table, String guid)
	{
		Item retval = null;
		table = Configuration.DB_TABLE_PREFIX+table;
		retval = Util.getTable(table).getItem("guid", guid);
		return((retval == null) ? new Item() : retval);
	}

	private Item getTimeslot(String guid)
	{
		return(getItem("timeslots", guid));
	}

	private Item getDuty(String guid)
	{
		return(getItem("duties", guid));
	}

	private String getWorker(String guid)
	{
		return(Util.fixJSON(getItem("workers", guid).toJSON()));
	}
}
