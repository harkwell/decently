package com.khallware.decently;

public class Role extends DecentlyRequestHandler implements Item
{
	public enum Keys { guid, name, description };

	public Role()
	{
		super("roles", Keys.values());
	}
}
