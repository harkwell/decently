package com.khallware.decently;

public class Duty extends DecentlyRequestHandler implements Item
{
	public enum Keys { guid, name, description, role_id };

	public Duty()
	{
		super("duties", Keys.values());
	}
}
