package com.khallware.decently;

public class Worker extends DecentlyRequestHandler implements Item
{
	public enum Keys { guid, firstname, lastname };

	public Worker()
	{
		super("workers", Keys.values());
	}
}
