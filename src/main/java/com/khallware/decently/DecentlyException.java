package com.khallware.decently;

public class DecentlyException extends Exception
{
	private static final long serialVersionUID = 0x0001L;

	public DecentlyException() {}

	public DecentlyException(String message)
	{
		super(message);
	}

	public DecentlyException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public DecentlyException(Throwable cause)
	{
		super(cause);
	}
}
