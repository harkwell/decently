package com.khallware.decently;

import com.khallware.decently.Util;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.dynamodbv2.document.Item;
import java.util.UUID;
import java.util.Map;

public class DecentlyRequestHandler
		implements RequestHandler<Map<String,Object>, String>
{
	protected String table = "";
	private Object[] keys = null;

	public DecentlyRequestHandler()
	{
		table = Configuration.DB_TABLE_PREFIX
			+this.getClass().getSimpleName() +"s";
	}

	public DecentlyRequestHandler(String table, Object[] keys)
	{
		this.table = Configuration.DB_TABLE_PREFIX + table;
		this.keys = keys;
	}

	public Map<String,Object> modifyContent(Map<String,Object> content)
			throws DecentlyException
	{
		content.put("guid", ""+UUID.randomUUID());
		return(content);
	}

	public void enforceValidation(Map<String,Object> content)
			throws DecentlyException
	{
		for (Object key1 : keys) {
			if (!content.containsKey(""+key1)) {
				throw new DecentlyException(
					"key required: "+key1);
			}
		}
		for (Object mapKey : content.keySet()) {
			boolean found = false;

			for (Object pattern : keys) {
				if ((""+pattern).equalsIgnoreCase(""+mapKey)) {
					found = true;
					break;
				}
			}
			if (!found) {
				throw new DecentlyException(
					"unknown json key: \""+mapKey+"\"");
			}
		}
	}

	@Override
	public String handleRequest(Map<String,Object> content, Context ctxt)
	{
		return(create(content, ctxt));
	}

	public String create(Map<String,Object> content, Context ctxt)
	{
		String rslt = "";
		String name = this.getClass().getSimpleName();
		Util.log(ctxt, "Item \""+name+"\" input: "+content);
		try {
			modifyContent(content);
			enforceValidation(content);
			Util.log(ctxt, "Posting to DynamoDB table: "+table);
			rslt = ""+Util.getTable(table).putItem(
				Item.fromMap(content));
		}
		catch (DecentlyException e) {
			rslt = "{ \"error\" : \""+e+"\" }";
		}
		Util.log(ctxt, "result: \""+rslt+"\"");
		return(rslt);
	}

	public String read(Context ctxt)
	{
		StringBuilder retval = new StringBuilder();
		boolean flag = false;
		Util.log(ctxt, "scanning table: \""+table+"\"");
		try {
			retval.append("{ \"");
			retval.append(table);
			retval.append("\" : [ ");

			for (Item item : Util.getTable(table).scan()) {
				retval.append((flag) ? ", " : "");
				retval.append(item.toJSON());
				flag = true;
			}
			retval.append("] }");
		}
		catch (Exception e) {
			retval.delete(0,retval.length());
			retval.append("{ \"error\" : \""+e+"\" }");
		}
		return(""+retval);
	}

	public String read(String guid, Context ctxt)
	{
		Item retval = Util.getTable(table).getItem("guid", guid);
		Util.log(ctxt, "result: \""+retval.toJSON()+"\"");
		return(retval.toJSON());
	}

	public String delete(String guid, Context ctxt)
	{
		String rslt = (guid == null || guid.isEmpty())
			? ""
			: ""+(Util.getTable(table).deleteItem("guid", guid));
		Util.log(ctxt, "result: \""+rslt+"\"");
		return(rslt);
	}

	@Override
	public String toString()
	{
		return(this.getClass().getSimpleName());
	}
}
