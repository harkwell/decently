package com.khallware.decently;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * RequestHandlerRouter - Handle API Gateway RESTful calls, route appropriately
 *
 * This class exists because mapping each method and uri in swagger results in
 * AWS copying the jar file once per method, thus increasing costs dramatically.
 *
 */
public class RequestHandlerRouter
		implements RequestHandler<Map<String,Object>, String>
{
	public static final String MAP_KEY1 = Configuration.HTTP_METHOD;
	public static final String MAP_KEY2 = Configuration.RESOURCE_PATH;
	public static final String MAP_KEY3 = Configuration.REST_PARMS;
	public static final String MAP_KEY4 = Configuration.BODY;
	public static final String MAP_KEY5 = Configuration.GUID;

	@Override
	public String handleRequest(Map<String,Object> content, Context ctxt)
	{
		String retval = "";
		try {
			retval = route(content, ctxt);
		}
		catch (Exception e) {
			retval = "{ \"error\" : \""+e+"\" }";
		}
		return(retval);
	}

	public String route(Map<String,Object> json, Context ctxt)
			throws IOException, DecentlyException
	{
		String retval = "";
		String method = ""+json.getOrDefault(MAP_KEY1, "unknown");
		String uri = ""+json.getOrDefault(MAP_KEY2, "unknown");
		String body = Util.toJson(json.get(MAP_KEY4));
		String guid = ""+json.getOrDefault(MAP_KEY5, "");
		DecentlyRequestHandler handler = pathToHandler(uri);
		Util.log(ctxt, "RESTful request: "+method+" "+uri);
		Util.log(ctxt, "Original json: "+body);
		Util.log(ctxt, "Handler: "+handler);
		Util.log(ctxt, Util.toJson(json.get(MAP_KEY3)));
		Util.log(ctxt, "Guid: "+guid);

		if (handler != null) {
			switch (method) {
			case "POST":
				retval = handler.create(
					Util.jsonToMap(body), ctxt);
				break;
			case "GET":
				retval = (guid == null || guid.isEmpty())
					? handler.read(ctxt)
					: handler.read(guid, ctxt);
				break;
			case "DELETE":
				retval = handler.delete(guid, ctxt);
				break;
			default:
				retval = "{ \"error\" : \"unhandled method: "
					+method+"\" }";
				break;
			}
		}
		return(retval);
	}

	public static DecentlyRequestHandler pathToHandler(String resourcePath)
	{
		DecentlyRequestHandler retval = null;
		String base = "";

		for (String token : resourcePath.split("/")) {
			if (!token.isEmpty()) {
				base = token.toLowerCase();
				break;
			}
		}
		switch (base) {
		case "services":
			retval = new Service();
			break;
		case "timeslots":
			retval = new Timeslot();
			break;
		case "roles":
			retval = new Role();
			break;
		case "duties":
			retval = new Duty();
			break;
		case "workers":
			retval = new Worker();
			break;
		case "service_timeslots":
			retval = new ServiceTimeslot();
			break;
		case "timeslot_duties":
			retval = new TimeslotDuty();
			break;
		case "worker_roles":
			retval = new WorkerRole();
			break;
		}
		return(retval);
	}
}
