package com.khallware.decently;

public class ServiceTimeslot extends DecentlyRequestHandler implements Item
{
	public enum Keys { guid, service_id, timeslot_id };

	public ServiceTimeslot()
	{
		super("service_timeslots", Keys.values());
	}
}
