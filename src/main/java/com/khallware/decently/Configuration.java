package com.khallware.decently;

public class Configuration
{
	public static final String DB_TABLE_PREFIX = "dctly_";

	// apigateway mapping template constants
	public static final String HTTP_METHOD = "context_httpmethod";
	public static final String RESOURCE_PATH = "context_resourcepath";
	public static final String REST_PARMS = "input_params";
	public static final String BODY = "input_body";
	public static final String GUID = "guid";
}
