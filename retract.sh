#!/usr/bin/bash

D="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

BUCKET=$1

[ -z "$BUCKET" ] && { echo "must specify bucket name!"; exit 1; }

aws s3 ls s3://$BUCKET 2>&1 >/dev/null || {
    echo "aws bucket must exist: s3://$BUCKET/"
    exit 1;
}
echo delete cloudformation stack
aws cloudformation delete-stack --stack-name decently
aws s3 rm --recursive s3://$BUCKET/html

echo delete cognito user pool
POOLID=$(aws cognito-idp list-user-pools --max-results 10 |jq '.UserPools[] |select(.Name == "decently") | .Id' |head -1 |sed 's#"##g')

[ "$POOLID" ] && {
   echo "        delete pool \"$POOLID\""
   aws cognito-idp delete-user-pool --user-pool-id $POOLID
}

echo delete api gateway apis
API_ID=$(aws apigateway get-rest-apis |jq '.items[] |select(.name == "Decently") | .id' |sed 's#"##g')

[ "$API_ID" ] && {
   echo "        api id: $API_ID"
   aws apigateway delete-rest-api --rest-api-id $API_ID
}

echo delete iam role
ROLEARN=$(aws iam list-roles |jq '.Roles[] |select(.RoleName == "decently-role") .Arn')

[ "$ROLEARN" ] && {
   echo "        role arn: $ROLEARN"
   aws iam detach-role-policy --role-name decently-role --policy-arn arn:aws:iam::aws:policy/AdministratorAccess
   aws iam delete-role --role-name decently-role
}

#echo drop the database
#
#for table in services timeslots duties timeslot_duties workers roles \
#      service_timeslots; do
#   aws dynamodb delete-table --table-name dctly_$table >/dev/null
#done
